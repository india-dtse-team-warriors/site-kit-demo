package com.huawei.hms.site.sample;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.huawei.hms.site.api.model.Site;
import com.huawei.hms.site.widget.SearchIntent;

public class WidgetIntentActivity extends AppCompatActivity {

    private SearchIntent searchIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_intent);
        searchIntent = new SearchIntent();
        searchIntent.setApiKey("CV8FlRHPSCcQ1wfDufiD%2FEmECZdWwbIMLt3%2BojRlGd6Z29lykaGQMFLnhw5AgFnp1v59icy65IxElEtFC%2FFFTtHCN%2F7H");
        Intent intent = searchIntent.getIntent(this);
        startActivityForResult(intent, SearchIntent.SEARCH_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (searchIntent.SEARCH_REQUEST_CODE == requestCode) {
            if (searchIntent.isSuccess(resultCode)) {
                Site site;
                site = searchIntent.getSiteFromIntent(data);
                Toast.makeText(getApplication(), site.getName(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
