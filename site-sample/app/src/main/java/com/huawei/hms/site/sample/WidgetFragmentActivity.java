package com.huawei.hms.site.sample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.huawei.hms.site.api.model.SearchStatus;
import com.huawei.hms.site.api.model.Site;
import com.huawei.hms.site.widget.SearchFragment;
import com.huawei.hms.site.widget.SiteSelectionListener;

public class WidgetFragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_fragment);
        SearchFragment fragment = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.widget_fragment);
//设置SearchFragment的API key
        fragment.setApiKey("CV8FlRHPSCcQ1wfDufiD%2FEmECZdWwbIMLt3%2BojRlGd6Z29lykaGQMFLnhw5AgFnp1v59icy65IxElEtFC%2FFFTtHCN%2F7H")
        fragment.setOnSiteSelectedListener(new SiteSelectionListener() {
            @Override
            public void onSiteSelected(Site data) {
//                Toast.makeText(getApplication(), data.getName(), Toast.LENGTH_LONG).show();
                Log.i("WidgetFragmentActivity", "hello");
                Toast.makeText(getApplication(), data.toString(), Toast.LENGTH_LONG).show();
            }
            @Override
            public void onError(SearchStatus status) {
                Toast
                        .makeText(getApplication(), status.getErrorCode() + "\n" + status.getErrorMessage(),
                                Toast.LENGTH_LONG)
                        .show();
            }
        });
    }
}
