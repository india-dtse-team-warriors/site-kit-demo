package com.huawei.hms.site.sample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.huawei.hms.site.api.model.SearchStatus;
import com.huawei.hms.site.api.model.Site;
import com.huawei.hms.site.widget.SearchFragment;
import com.huawei.hms.site.widget.SiteSelectionListener;

public class WidgetActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget);
        findViewById(R.id.widget_fragment).setOnClickListener(this);
        findViewById(R.id.widget_intent).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.widget_fragment:
                startActivity(new Intent(this, WidgetFragmentActivity.class));
                break;
            case R.id.widget_intent:
                startActivity(new Intent(this, WidgetIntentActivity.class));
                break;
            default:
                break;
        }
    }
}
