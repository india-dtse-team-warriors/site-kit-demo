package com.huawei.hms.site.sample;

import androidx.appcompat.app.AppCompatActivity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class GeoCoderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_coder);
        List<Address> result = new ArrayList<>();
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        double lon = 103.845463;
        double lat = 1.296056;
        try {
            result = geocoder.getFromLocation(lat, lon, 1);
            Log.i("GEO", result.get(0).toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
